function [output]= allianceGenerator2(playerMatrix,threatMatrix, outcomeMatrix, phase, equationWeights)

numPlayers=size(playerMatrix,1);
permVector=1:numPlayers;
permMatrix=dec2bin(2^numPlayers-1:-1:0)-'0';

tempMatrix=[];
tempDeviations=0;

%outputs
bestAllianceMatrix=[];
alliance1Size=0;
minDeviations=numPlayers^2;
rowHeaders=[];

%submatrices
topLeftPos=[];
bottomRightPos=[];
topRightNeg=[];
bottomLeftNeg=[];

for i=1:(size(permMatrix,1))
    %create permuation of player matrix by switching around columns
    allianceSize=sum(permMatrix(i,:) == 1);
    if(allianceSize==0 || allianceSize==numPlayers)
        continue;
    end
    allianceMatrixOrder=[];
    for j=1:numPlayers
        if(permMatrix(i,j)==1)
            allianceMatrixOrder=[j allianceMatrixOrder()];
        else
            allianceMatrixOrder=[allianceMatrixOrder() j];
        end
    end    
    tempMatrix=playerMatrix([allianceMatrixOrder(:)],[allianceMatrixOrder(:)]);
    
    %divide in quadrants based on size of alliance 1 and count deviations
     topLeftPos=tempMatrix(1:allianceSize,1:allianceSize);
     bottomRightPos=tempMatrix(allianceSize+1:numPlayers,allianceSize+1:numPlayers);
     topRightNeg=tempMatrix(1:allianceSize, allianceSize+1:numPlayers);
     bottomLeftNeg=tempMatrix(allianceSize+1:numPlayers,1:allianceSize);

       
     tempDeviations=numNeg(topLeftPos)+numNeg(bottomRightPos)+numPos(topRightNeg)+numPos(bottomLeftNeg);
     %tempDeviations=numNotStrat(topLeftPos)+numNotStrat(bottomRightPos)+numStrat(topRightNeg)+numStrat(bottomLeftNeg);
     %TODO:perhaps test all equally good alliance combos to improve accuracy
     if tempDeviations<minDeviations
        bestAllianceMatrix=tempMatrix;
        alliance1Size=allianceSize;
        minDeviations=tempDeviations;
        rowHeaders=transpose(allianceMatrixOrder(:));
     end
end
  %%%disp(rowHeaders)
  %%%disp(bestAllianceMatrix)
  %%%disp('Alliance 1 Size:')
  %%%disp(alliance1Size)
  %%%disp('Number of Deviations:')
  %%%disp(minDeviations)
  
  alliance1IsMajority=0;
  if(alliance1Size==numPlayers/2)
      %%%disp('No majority alliance')
      alliance1IsMajority=0;
  else if (alliance1Size>=ceil(numPlayers/2))
          %%%disp('Alliance 1 has the majority')
          alliance1IsMajority=1;
      else
          %%%disp('Alliance 2 has the majority')
          alliance1IsMajority=2;
      end
  end
  
  
  alliance1Matrix=bestAllianceMatrix(1:alliance1Size,1:alliance1Size);
  alliance2Matrix=bestAllianceMatrix(alliance1Size+1:numPlayers,alliance1Size+1:numPlayers);
  alliance1FeelingsTowardsAlliance2=bestAllianceMatrix(alliance1Size+1:numPlayers,1:alliance1Size);
  alliance2FeelingsTowardsAlliance1=bestAllianceMatrix(1:alliance1Size, alliance1Size+1:numPlayers);
  %disp(alliance1Size);
 % disp(numPlayers);
  %TODO:find potential flippers (add up columns in alliance matrices to find least connected person(s)
  %neededNumberOfFlippers=ceil(numPlayers/2)-alliance1Size;
  %if(neededNumberOfFlippers<=0)
     %alliance1 has majority, alliance2 needs abs(neededNumberOfFlippers)+1 to have majority 
  %else
      %allaince 2 has majority, allaince 1 needs abs(neededNumberOfFlippers) to get majority
 % end
  
  %find target of each alliance with and without flippers
  %use the four phases and the different threat scores and add weights to
  %characteristics
  %disp(size(equationWeights));
  strengthWeight=equationWeights(1);
  weaknessWeight=equationWeights(2);
  strategicWeight=equationWeights(3);
  socialWeight=equationWeights(4);
  
  biggestThreatScore1=intmin;%((numPlayers+1)+10)*-3;%this might need to be updated once weights are assigned
  biggestThreat1=[];
  biggestThreatScore2=intmin;%((numPlayers+1)+10)*-3;%this might need to be updated once weights are assigned
  biggestThreat2=[];
  socialScores=sum(alliance2FeelingsTowardsAlliance1,2);
  for i=1:size(alliance1Matrix,1)
      %checking for immunity
      if(outcomeMatrix(rowHeaders(i),numPlayers+3)==1)
         continue; 
      end
      %disp('player number');
      %disp(size(alliance2FeelingsTowardsAlliance1));
      %disp('i='+i+';'+rowHeaders(i)+';'+size(threatMatrix)+';'+size(socialScores))
      threatScore=threatMatrix(1,rowHeaders(i))*strategicWeight+(threatMatrix(2,rowHeaders(i))+threatMatrix(6,rowHeaders(i)))*strengthWeight+(threatMatrix(3,rowHeaders(i))+threatMatrix(7,rowHeaders(i)))*weaknessWeight+(socialScores(i))*socialWeight;
    % disp('--------');
%       if(i==1)
%          biggestThreatScore1=threatScore; 
%          biggestThreat1=[];
%          biggestThreat1(1)=rowHeaders(1,i);
%          continue;
%       end
      if(threatScore>biggestThreatScore1)
         biggestThreatScore1=threatScore;
         biggestThreat1=[];
         biggestThreat1(1)=rowHeaders(1,i);
         %disp('new biggest threat');
      else if(threatScore==biggestThreatScore1)
              %determine who breaks a tie with idols or other factors
              biggestThreat1=[biggestThreat1, rowHeaders(1,i)];
              %disp('equal biggest threat');
          end
      end
  end
  if(size(biggestThreat1,1)>1)
     %decide which threat to target
     canSplitVote=false;
     if(size(alliance2Matrix,1)>=(size(alliance1Matrix,1)*2-2))
         canSplitVote=true;
     end
     for i=1:size(biggestThreat1,1)
        if(threatMatrix(4,biggetThreat1(i))==1 && canSplitVote)%has idol
            biggestThreat1=[];
            biggestThreat1(1)=rowHeaders(1,i);
            break;
        else if(threatMatrix(4,biggestThreat1(i))==1 && ~canSplitVote)
               continue;
            else if(true)%TODO:fill in condition that checks dislike from other alliance
                    biggestThreat1=[];
                    biggestThreat1(1)=rowHeaders(1,i); 
                end
                
            end
            
        end
     end
  end
  
 socialScores=sum(alliance1FeelingsTowardsAlliance2,2);
  for i=1:size(alliance2Matrix,1)
      %checking for immunity
      %disp(size(alliance2Matrix,1));
      if(outcomeMatrix(rowHeaders(i+size(alliance1Matrix,1)),numPlayers+3)==1)
          %disp('immunity');
         continue; 
      end
      %disp(i+size(alliance1Matrix,1));
      %disp(size(socialScores));
%       if(i==1)
%          biggestThreatScore2=threatScore; 
%          biggestThreat1=[];
%          biggestThreat2(1)=rowHeaders(1,i+size(alliance1Matrix,1));
%          continue;
%       end
      threatScore=threatMatrix(1,rowHeaders(i+size(alliance1Matrix,1)))*strategicWeight+(threatMatrix(2,rowHeaders(i+size(alliance1Matrix,1)))+threatMatrix(6,rowHeaders(i+size(alliance1Matrix,1))))*strengthWeight+(threatMatrix(3,rowHeaders(i+size(alliance1Matrix,1)))+threatMatrix(7,rowHeaders(i+size(alliance1Matrix,1))))*weaknessWeight+(-1*socialScores(i))*socialWeight;
%       disp('------');
%       disp(threatScore);
%       disp(biggestThreatScore2);
      if(threatScore>biggestThreatScore2)
         biggestThreatScore2=threatScore;
         biggestThreat2=[];
         biggestThreat2(1)=rowHeaders(1,i+size(alliance1Matrix,1));
      else if(threatScore==biggestThreatScore2)
              %determine who breaks a tie with idols or other factors
              biggestThreat2=[biggestThreat2, rowHeaders(1,i+size(alliance1Matrix,1))];
          end
      end
  end
  if(size(biggestThreat2,1)>1)
     %decide which threat to target
     canSplitVote=false;
     if(size(alliance1Matrix,1)>=(size(alliance2Matrix,1)*2-2))
         canSplitVote=true;
     end
     for i=1:size(biggestThreat2,1)
        if(threatMatrix(4,biggetThreat2(i+size(alliance1Matrix,1)))==1 && canSplitVote)%has idol
            biggestThreat2=[];
            biggestThreat2(1)=rowHeaders(1,i+size(alliance1Matrix,1));
            break;
        else if(threatMatrix(4,biggestThreat2(i+size(alliance1Matrix,1)))==1 && ~canSplitVote)
               continue;
            else if(true)%TODO:fill in condition that checks dislike from other alliance
                   biggestThreat2=[];
                   biggestThreat2(1)=rowHeaders(1,i+size(alliance1Matrix,1)); 
                end
                
            end
            
        end
     end
  end
  output=false;
  %%%disp('The biggest threat in alliance 1 is:')
  %%%disp(biggestThreat1)
%   disp(size(outcomeMatrix));
%   disp(size(biggestThreat2));
%   disp(numPlayers+4);
  if(alliance1IsMajority==1 && size(biggestThreat2,1)==0 && outcomeMatrix(biggestThreat1(1), numPlayers+4)==1)
      output=true;
    else if(alliance1IsMajority==1 && size(biggestThreat2,1)==0)
        output=false;
       else if(alliance1IsMajority==2 && size(biggestThreat1,1)==0 && outcomeMatrix(biggestThreat2(1), numPlayers+4)==1)
              output=true;
          else if(alliance1IsMajority==2 && size(biggestThreat1,1)==0)
                output=false;
             else if(alliance1IsMajority==1 && outcomeMatrix(biggestThreat2(1), numPlayers+4)==1)
                 output=true;
                  else if(alliance1IsMajority==1 && outcomeMatrix(biggestThreat2(1), numPlayers+2)==1 && outcomeMatrix(biggestThreat1(1), numPlayers+4)==1)
                         output=true;
                      else if(alliance1IsMajority==2 && outcomeMatrix(biggestThreat1(1), numPlayers+4)==1)
                             output=true; 
                          else if(alliance1IsMajority==2 && outcomeMatrix(biggestThreat1(1), numPlayers+2)==1 && outcomeMatrix(biggestThreat2(1), numPlayers+4)==1)
                                 output=true; 
                              else if(alliance1IsMajority==0 && (outcomeMatrix(biggestThreat2(1), numPlayers+4)==1 || outcomeMatrix(biggestThreat1(1), numPlayers+4)==1))
                                 output=true; 
                                  end
                              end
                          end
                      end 
                 end 
              end 
           end 
        end 
  end 
  
  
  
  %determine which possible outcome is most likely
end


