function [numStrat]=numStrat(matrix)
    temp=0;
    for i=1:size(matrix,1)
        for j=1:size(matrix,2)
            if matrix(i,j)==3 || matrix(i,j)==2 || matrix(i,j)==-1
                temp=temp+abs(matrix(i,j));
            end
        end
    end
    numStrat=temp;
end