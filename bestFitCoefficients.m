function []=bestFitCoefficients(data)
    fileID = fopen('output.txt','a');
    %coefficients=[-1,-.9,-.8,-.7,-.6,-.5,-.4,-.3,-.2,-.1,0,.1,.2,.3,.4,.5,.6,.7,.8,.9,1];
    coefficients=[-1,-.8,-.6,-.4,-.2,0,.2,.4,.6,.8,1];
    %coefficients=[-1,0,1];
    permutations=permn(coefficients,4);
    %permutations=unique(permutations,'rows');
    %disp(permutations(1,:));
    maxSuccesses=0;
    permSize=size(permutations,1);
    %disp(permSize);
    %return;
    bestCoefficients=[];
    %disp(size(permutations,1));
    %return;
    for phase=1:4
        for i=1:size(permutations,1)
            disp([int2str(phase),'.',int2str(i),' / ', int2str(permSize)]);
            %tempMatrix=vec2mat(permuations(i),4);
            tempMatrix=permutations(i,:);
            %disp(size(tempMatrix))
            numOfSuccesses=runTheDataOnce(tempMatrix,data,phase);
            if(numOfSuccesses>maxSuccesses)
                bestCoefficients=[];
                %disp(permutations(i,:));
                %disp(size(permutations(i,:)));
                %disp(size(bestCoefficients));
                bestCoefficients=permutations(i,:);
                maxSuccesses=numOfSuccesses;
            else if(numOfSuccesses==maxSuccesses)
                   bestCoefficients=[bestCoefficients; permutations(i,:)]; 
                end
            end
        end
        totalNumberOfDataPoints=[15,12,15,14];
        fprintf(fileID, 'strategy vs non strategy alliances');
        fprintf(fileID,'phase: %d\n',phase);
        fprintf(fileID,'num of Successes: %d\n',maxSuccesses);
        fprintf(fileID,'total number of data points: %d\n',totalNumberOfDataPoints(phase));
        fprintf(fileID,'percent correct: %.2f\n',((maxSuccesses/totalNumberOfDataPoints(phase))*100));
        fprintf(fileID,'num of best coefficients: %d\n',size(unique(bestCoefficients,'rows'),1));
        fprintf(fileID,'strength: %.1f weakness: %.1f strategic: %.1f social: %.1f\n',unique(bestCoefficients,'rows'));
        fprintf(fileID,'-------------------------------------------------------------------------------\n');
        %disp(['phase',int2str(phase)]);
        %disp(['num of successes:',int2str(maxSuccesses)]);
        %disp(['total number of data points:',int2str(size(data, 2))]);
        %disp(['num of best coefficients: ',int2str(size(bestCoefficients,1))]);
        %disp('coefficients: ');
        %disp(bestCoefficients);
        
        maxSuccesses=0;
        bestCoefficients=[];
        %break;
    end
    fclose(fileID);
    %disp('num of successes:'+maxSuccesses+'\ntotal number of data points:'+size(data, 3)+'\ncoefficients:\n'+bestCoefficients);   
end
