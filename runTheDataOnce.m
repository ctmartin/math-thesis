function [numberOfSuccesses]= runTheDataOnce(equationWeights, data,phaseNumber)
    phase=[1,1,1,1,2,2,3,3,3,3,3,4,4,4,4,1,1,1,1,2,2,3,3,3,3,4,4,4,1,1,1,1,2,2,2,2,2,3,3,3,4,4,4,1,1,1,2,2,2,3,3,3,4,4,4];
    numberOfSuccesses=0;
    
    [w,x,y,z]=size(data);
    totalNumber=x;
   % disp(['totalNumber: ',int2str(size(data))]);
    %run get data point with equationWeights
    for n=1:totalNumber
        %disp(size(phase))
        if(phase(n)~=phaseNumber)
           continue; 
        end
        %disp(size(data{n}))
        if(allianceGenerator2(data{n}{1},data{n}{2},data{n}{3},phase(n),equationWeights)==true)
           numberOfSuccesses=numberOfSuccesses+1;
        end
    end
    percentCorrect=(numberOfSuccesses/totalNumber)*100;
    %disp(totalNumber)
    %disp(['Number Correct=',num2str(numberOfSuccesses)])
    %disp(['Percent Correct=',num2str(percentCorrect)])
end