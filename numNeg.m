function [numNeg]=numNeg(matrix)
    temp=0;
    for i=1:size(matrix,1)
        for j=1:size(matrix,2)
            if matrix(i,j)<0
                temp=temp+abs(matrix(i,j));
            end
        end
    end
    numNeg=temp;
end